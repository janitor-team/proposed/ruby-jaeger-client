# frozen_string_literal: true

module Jaeger
  module Client
    VERSION = '1.2.0'
  end
end
